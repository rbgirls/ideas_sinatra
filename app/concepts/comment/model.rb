module MyIdeas
  class Comment < Base::Model
    set_dataset :comments
    many_to_one :idea
  end
end
