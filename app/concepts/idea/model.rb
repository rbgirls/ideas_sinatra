module Idea
  class Model < Base::Model
    set_dataset :ideas
    one_to_many :comments
  end
end
