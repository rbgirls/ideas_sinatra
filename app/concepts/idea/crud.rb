module Idea

  class Index
    def self.call(params={})
      @ideas = Model.all
    end
  end

  class Create
    def self.call(params)
      @idea = Model.create(params[:idea])
    end
  end

  class Read
    def self.call(params)
      @idea = Model.find(params[:id])
    end
  end

  class Update
    def self.call(params)
      @idea = Read.(params)
      @idea.update(params[:idea])
    end
  end

  class Destroy
    def self.call(params)
      puts params[:id]
      @idea = Read.(params)
      @idea.delete
    end
  end

end
