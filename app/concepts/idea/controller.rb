module Idea
  class Controller < Base::Controller
    set :views, File.join(APP_ROOT, 'app', 'concepts', 'idea', 'views')

    get '/ideas/?' do
      context = {ideas: Idea::Index.()}

      request.accept.each do |type|
        case type.to_s
        when 'text/html'
          halt haml(:index, :locals => context)
        when 'application/json'
          halt context.to_json
        # when 'application/atom+xml'
        #   halt nokogiri(:'index.atom', :locals => context)
        # when 'application/xml', 'text/xml'
        #   halt nokogiri(:'index.xml', :locals => context)
        when 'text/plain'
          halt context.to_s
        end
      end
      error 406
    end

    post '/ideas' do
      Idea::Create.(params)
      redirect "/ideas/#{params[:id]}"
    end

    get '/ideas/new/?' do
      haml :new, locals: {idea: Model.new}
    end

    post '/ideas/:id/?' do
      Idea::Update.(params)
      redirect "/ideas/#{params[:id]}"
    end

    get '/ideas/:id/?' do
      context = {idea: Idea::Read.(params)}

      request.accept.each do |type|
        case type.to_s
        when 'text/html'
          halt haml(:show, :locals => context)
        when 'text/json'
          halt context.to_json
        when 'text/plain'
          halt context.to_s
        end
      end
      error 406
    end

    get '/ideas/:id/edit/?' do
      context = {idea: Idea::Read.(params)}
      haml :edit, locals: context
    end

    get '/ideas/:id/delete/?' do
      @idea = Idea::Destroy.(params)
      redirect :ideas
    end
  end
end
