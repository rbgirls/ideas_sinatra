require 'rubygems'
require 'bundler'

APP_ROOT = File.dirname(__FILE__)

# Setup load paths
Bundler.require
$: << File.expand_path('../', __FILE__)
$: << File.expand_path('../lib', __FILE__)
# Require base
require 'sinatra/base'
require 'active_support'
require 'active_support/core_ext'
require 'active_support/json'

DB = Sequel.connect('sqlite://db/my_ideas_dev.db') # FIXME
Dir['lib/**/*.rb'].sort.each { |file| require file }
Dir['app/concepts/**/*.rb'].sort.each { |file| require file }

module MyIdeas
  class App < Sinatra::Application
    use Idea::Controller
  end
end
